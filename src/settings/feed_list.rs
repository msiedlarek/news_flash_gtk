use serde::{Deserialize, Serialize};
use std::default::Default;

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct FeedListSettings {
    pub only_show_relevant: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub width: Option<u32>,
}
